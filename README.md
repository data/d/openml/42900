# OpenML dataset: breast-cancer-coimbra

https://www.openml.org/d/42900

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Miguel Patricio, Jose Pereira, Joana Crisostomo, Paulo Matafome, Raquel Seica, Francisco Caramelo
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Coimbra) - 2018
**Please cite**: [Paper](doi:10.1186/s12885-017-3877-1  Heart failure clinical records dataset 

**Breast cancer coimbra dataset**

There are 10 predictors, all quantitative, and a binary dependent variable, indicating the presence or absence of breast cancer. The predictors are anthropometric data and parameters which can be gathered in routine blood analysis. Prediction models based on these predictors, if accurate, can potentially be used as a biomarker of breast cancer.

### Attribute information

Quantitative Attributes: 
- Age (years) 
- BMI (kg/m2) 
- Glucose (mg/dL) 
- Insulin (microgram/mL) 
- HOMA 
- Leptin (ng/mL) 
- Adiponectin (microg/mL) 
- Resistin (ng/mL) 
- MCP-1(pg/dL) 

Labels: 

1. Healthy controls 
2. Patients

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42900) of an [OpenML dataset](https://www.openml.org/d/42900). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42900/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42900/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42900/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

